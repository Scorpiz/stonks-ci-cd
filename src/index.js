const app = require('express')();

app.get('/', (_, res) => {
	res.send('Hello World!');
});

app.listen(3000, () => {
	console.log('app is listening on port 3000');
});
